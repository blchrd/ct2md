# CherryTree to MarkDown

Script to convert a CherryTree notebook to Markdown file.

For now, it is a "for my usage" script, so I didn't cover all the features of CherryTree right now (e.g. pinned node, link to another node, ...). You have the Todo list below.

Note that this script is not optimized. It just works (on my machine anyway).

## Installation

```
git clone https://framagit.org/blchrd/ct2md
cd ct2md
pip install -r requirements.txt
```

## Usage

```
python ct2md.py <path to CherryTree notebook> [<export root directory>]
```

If you want to convert an encrypted notebook, you'll need to have [7zip](https://www.7-zip.org/) installed and on your path. In the other case, you have to save your notebook in a clear format.

*All converted files will be outputed in `output` directory by default, you can change this dir with the export_dir parameters.*

## ToDo list

### Format

- [x] Unencrypted XML
- [x] Encrypted XML
- [x] Unencrypted SQLite
- [x] Encrypted SQLite
- [ ] HTML export

### CherryTree feature

- [ ] Link to other node
- [ ] Pinned node
- [ ] Table
- [ ] SQLite Image
- [ ] SQLite CodeBox