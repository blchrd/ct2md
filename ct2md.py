import sys
import base64
import os
import untangle
import subprocess
import getpass
from bs4 import BeautifulSoup
import sqlite3

if len(sys.argv) == 1:
    print("Usage: ct2md.py <CherryTree notebook path> [<export root directory>]")
    exit(0)

export_root = "output/"
input_file = sys.argv[1]
if len(sys.argv) > 2:
    export_root = sys.argv[2] + "/"


def getSqliteCursor(connection):
    return connection.cursor()


def mkdir(path):
    if not os.path.exists(path):
        os.makedirs(path)


def richtext_to_markdown(node_content, node_title):
    markdown = node_content.cdata
    formated = False
    prefix = ""
    suffix = ""
    
    if node_content._name == "rich_text":
        if node_content["scale"] == 'h1':
            prefix = "# "
        elif node_content["scale"] == 'h2':
            prefix = "## "
        elif node_content["scale"] == 'h3':
            prefix = "### "

        if node_content["weight"] == "heavy":
            prefix = "**"
            suffix = "**"

        if node_content["strikethrough"] == "true":
            prefix = "~~"
            suffix = "~~"

        if node_content["style"] == "italic":
            prefix = "*"
            suffix = "*"

        if node_content["link"] != None:
            if node_content["link"].startswith("webs "):
                url = node_content["link"].replace("webs ", "")
                markdown = f'[{markdown}]({url})'
            formated = True

        markdown = markdown.replace("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~", "---")
        if prefix != "" or suffix != "":
            formated = True
    elif node_content._name == "codebox":
        formated = True
        language = node_content["syntax_highlighting"]
        if language == None:
            language = ''

        prefix = f'```{language}\n'
        suffix = '\n```'
    elif node_content._name == "encoded_png":
        included_file = ""
        if node_content["filename"] != None:
            included_file = node_title+"-files/"+node_content["filename"]
            markdown = f"[{included_file}]({included_file})\n"
        else:
            included_file = node_title+"-files/"+node_content["char_offset"]+".jpg"
            markdown = f"![{included_file}]({included_file})\n"

    if not formated:
        markdown = markdown.replace("\n", "\n\n")
        # Remove double linebreak for the list
        markdown = markdown.replace("\n• ", "* ")
        markdown = markdown.replace("\n- ", "* ")
    markdown = prefix+markdown+suffix

    return markdown


def extractEncryptedNotebook(file_path, extract_dir):
    password = getpass.getpass('Notebook is encrypted, please enter the password: ')
    subprocess.call(["7z", "x", '-p{}'.format(password), file_path, "-o{}".format(extract_dir)])


def parse_xml_node(node, root_dir, index=0):
    has_children = False
    node_name = node["name"]
    sub_directory = f'{root_dir}{node_name}/'
    
    for subnode in node.children:
        if subnode._name == "node":
            mkdir(sub_directory)
            has_children = True
            parse_xml_node(subnode, sub_directory, index+1)

    if has_children:
        note_path = f'{sub_directory}/{node_name}.md'
        note_assets_path = f'{sub_directory}/{node_name}-files'
    else:
        note_path = f'{root_dir}{node_name}.md'
        note_assets_path = f'{root_dir}{node_name}-files'

    note_content = f"# {node_name}\n\n"
    has_content = False
    for text_node in node.children:
        if text_node._name != "node":
            if text_node._name == "encoded_png":
                # Create files subdirectory
                mkdir(note_assets_path)

                # File (image or other)
                b64_encoded_file = text_node.cdata
                if text_node["filename"] != None:
                    with open(note_assets_path+"/"+text_node["filename"], 'wb') as included_file:
                        included_file.write(base64.b64decode(b64_encoded_file))
                        included_file.close()
                else:
                    # Without filename, we assume it is an image
                    with open(note_assets_path+"/"+text_node["char_offset"]+".jpg", 'wb') as included_file:
                        included_file.write(base64.b64decode(b64_encoded_file))
                        included_file.close()
                    
            has_content = True
            note_content += richtext_to_markdown(text_node, node_name)+"\n"

    if has_content:
        with open(note_path, 'w', encoding='utf-8') as note_file:
            note_file.write(note_content)


def parse_sqlite_node(node_id, root_dir, index=0):
    cursor = getSqliteCursor(conn)
    children = cursor.execute("select node_id from children where father_id = ?", [node_id]).fetchall()
    print(node_id, len(children))
        
    sql_data = cursor.execute("select txt, name, has_codebox, has_image, has_table from node where node_id = ?", [node_id]).fetchall()[0]
    xml_text = sql_data[0]
    node_name = sql_data[1]
    sub_directory = f'{root_dir}{node_name}/'

    if len(children) > 0:
        mkdir(sub_directory)
        note_path = f'{sub_directory}/{node_name}.md'
        note_assets_path = f'{sub_directory}/{node_name}-files'
        for row in children:
            parse_sqlite_node(node_id=row[0], root_dir=sub_directory)
    else:
        note_path = f'{root_dir}{node_name}.md'
        note_assets_path = f'{root_dir}{node_name}-files'

    rootnode = untangle.parse(xml_text)
    note_content = ""
    for node_content in rootnode.node:
        if len(node_content.children) > 0:
            for text_node in node_content.children:
                note_content += richtext_to_markdown(text_node, node_name)+"\n"

    # Codebox
    if sql_data[2]:
        pass
    # Image
    if sql_data[3]:
        pass
    # Table
    if sql_data[4]:
        pass

    if note_content != "":
        # Creation of the note
        with open(note_path, 'w', encoding='utf-8') as note_file:
            note_file.write(note_content)



# Create export directory
mkdir(export_root)

# Depends on input format, we try to convert
if os.path.isfile(input_file):
    # it is a cherrytree notebook
    encrypted = False
    sqlite = False
    
    filename = os.path.basename(input_file)
    filename, file_extension = os.path.splitext(filename)
    file_dir = os.path.dirname(os.path.realpath(input_file))

    # CherryTree encryption consist of a encrypted zip archive
    if file_extension == ".ctz":
        # xml file encrypted
        encrypted = True
        extractEncryptedNotebook(input_file, file_dir)
        input_file = f'{file_dir}/{filename}.ctd'
        file_extension = ".ctd"
    elif file_extension == ".ctx":
        # sqlite database encrypted
        encrypted = True
        extractEncryptedNotebook(input_file, file_dir)
        input_file = f'{file_dir}/{filename}.ctb'
        file_extension = ".ctb"

    # ctb or ctd (we didn't handle encrypted notebook yet)
    if file_extension == ".ctd":
        # xml file
        xml_file = untangle.parse(input_file)
        for rootnode in xml_file.cherrytree.node:
            parse_xml_node(rootnode, export_root)
    elif file_extension == ".ctb":
        sqlite = True
        conn = sqlite3.connect(input_file)
        # sqlite database
        for row in getSqliteCursor(conn).execute("select node_id from children where father_id = 0"):
            parse_sqlite_node(node_id=row[0], root_dir=export_root)

    if encrypted:
        if sqlite:
            conn.close()
        # Delete the extracted notebook
        os.remove(input_file)
elif os.path.isdir(input_file):
    # html export directory
    print("html export")
